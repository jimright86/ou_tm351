#!/bin/bash
for pkg in `cat TM351_VM_Python_Packages.txt` 
do 
	echo "================";
	echo $pkg; 
	echo "================";
	deps=`pip show ${pkg/==*/} | grep "Requires:"`
	for vm_only_pkgs in `cat tm351vm_only.csv`
	do
		echo $deps | grep -q $vm_only_pkgs
		if [ $? == 0 ]
		then
			echo "Requires $vm_only_pkgs"
		fi
	done
done
