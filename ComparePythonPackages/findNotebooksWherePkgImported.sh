#!/bin/bash
for pkg in `cat tm351vm_only.csv` 
do 
echo "====================="
echo Search for Notebooks that use $pkg
echo "====================="
grep -iR $pkg ../notebooks/testRun_2019-0*/*/*.ipynb
# replace all -'s with _'s and search again
pkg_=${pkg//-/_g}
if [ "$pkg_" != "$pkg" ]; then
	grep -iR $pkg_ ../notebooks/testRun_2019-0*/*/*.ipynb 
fi
done
