#!/bin/bash
# Test notebooks with nbval

echo "**** Running nbval ****"
py.test -v -s --nbval examples/

echo "**** Running nbval-lax ****"
py.test -v -s --nbval-lax examples/

